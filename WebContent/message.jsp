<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>新規投稿画面</title>
</head>
<body>

	<div class="form-area">

			<a href="./">ホーム</a>
			<form action="message" method="post">

				<label for="title">件名</label>
                <input name="title" id="title" value="${message.title}"/> <br />

                <label for="category">カテゴリ</label>
                <input name="category" id="category" value="${message.category}"/> <br />

				<label for="text">本文</label>
				<textarea name="text" id="text">${message.text}</textarea><br />

				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="errorMessage">
								<li><c:out value="${errorMessage}" />
							</c:forEach>
						</ul>
					</div>
				<c:remove var="errorMessages" scope="session" />
				</c:if>
				<br /><input type="submit" value="投稿">
			</form>
	</div>

	<div class="copyright">Copyright(c) Ren Miyamoto</div>
</body>
</html>