<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/homeStyle.css" rel="stylesheet" type="text/css">
<title>ホーム画面</title>
<script type="text/javascript">

function disp(){

	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if(window.confirm('本当に投稿を削除しますか？')){
		return true;
	}
	// 「キャンセル」時の処理開始
	else{
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false
	}
}
</script>

</head>
<body>

	<div class="main-contents">
		<div class="header">
			<a href="message">新規投稿</a>
			<!-- ここに条件を入れる -->
			<c:if test="${loginUser.branch == 1 || loginUser.department == 1}">
				<a href="management">ユーザー管理</a>
			</c:if>
			<!-- end -->
			<a href="logout">ログアウト</a>
		</div>
	</div>
	<br>
	<!-- ManagementFilterのmanagementErrorMessagesに値がセットされている場合 -->
    <c:if test="${ not empty managementErrorMessages }">
    <div class="filterErrorMessages">
    	<c:out value="${managementErrorMessages}" />
    </div>
    <br>
    </c:if>


	<div class="narrowedDown">
		<form action="./" method="get" class="narrowedDown">
		<div class="narrowedDown1">
			<label for="start">日付</label>
			<input type="date" name="start" id="start" value="${optedStart}">
			<p>~</p>
			<input type="date" name="end" id="end" value="${optedEnd}">
		</div>
		<div class="narrowedDown2">
			<label for="category">カテゴリ</label>
			<input type="text" name="category" id="category" value="${optedCategory}">
			<input type="submit" value="絞込み">
		</div>
		</form>
	</div>
	<br>
	<c:if test="${ not empty messages }">
		<div class="messages-container">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="message-element">
						<p>ユーザ名</p>
						<c:out value="${message.account}" />
					</div>
					<div class="message-element">
						<p>件名</p>
						<c:out value="${message.title}" />
					</div>
					<div class="message-element">
						<p>カテゴリー</p>
						<c:out value="${message.category}" />
					</div>
					<div class="message-element">
						<p>投稿内容</p>
						<c:forEach var="messageText"
							items="${fn:split(message.text, '
					')}">
							<div>${messageText}</div>
						</c:forEach>
					</div>
					<div class="message-element">
						<p>投稿日時</p>
						<fmt:formatDate value="${message.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
					<div class="message-element">
						<c:if test="${message.userId == loginUser.id}">
						<form action="deleteMessage" method="post" onsubmit="return disp()">
							<input name="id" value="${message.id}" id="id" type="hidden" />
							<input type="submit" value="削除">
						</form>
						</c:if>
					</div>
				</div>

				<div class="comments-container">
					<div class="comment">
						<!-- もし、投稿テーブルのidカラムとコメントテーブルのメッセージidが同じだったらコメントを表示させる -->
						<c:forEach items="${comments}" var="comment">
							<c:if test="${comment.messageId == message.id}">
								<div class="comment-element">
									<p>ユーザ名</p>
									<c:out value="${comment.account}" />
								</div>
								<div class="comment-element">
									<p>コメント内容</p>
										<c:forEach var="commentText" items="${fn:split(comment.text, '
										')}">
											<div>${commentText}</div>
										</c:forEach>
								</div>
								<div class="comment-element">
									<p>コメント日時</p>
									<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
								<div class="comment-element">
									<c:if test="${comment.userId == loginUser.id}">
										<form action="deleteComment" method="post" onsubmit="return disp()">
											<input type="hidden" name="id" value="${comment.id}"/>
											<input type="submit" value="削除">
										</form>
									</c:if>
								</div>
							</c:if>

						</c:forEach>
						<form action="comment" method="post">
							<input name="id" value="${message.id}" id="id" type="hidden" />
							<div class="comment-fw-element">
								<label for="text">コメント入力欄</label>
								<textarea name="text" id="text"></textarea>
							</div>
							<div class="comment-button-element">
								<input type="submit" value="コメント投稿">
							</div>

							<c:if test="${ not empty errorMessages }">
								<div class="errorMessages">
									<ul>
										<c:forEach items="${errorMessages}" var="errorMessage">
											<li><c:out value="${errorMessages}" />
										</c:forEach>
									</ul>
								</div>
								<c:remove var="errorMessages" scope="session" />
							</c:if>
						</form>
					</div>
					<br>
				</div>
			</c:forEach>
		</div>
	</c:if>

	<div class="copyright">Copyright(c) Ren Miyamoto</div>

</body>
</html>