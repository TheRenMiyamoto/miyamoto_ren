<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー管理画面	</title>
<script type="text/javascript">

function stop(){

	// 「停止ボタン押下」時の処理開始 ＋ 確認ダイアログの表示
	if(window.confirm('本当にアカウントを停止しますか？')){
		return true;
	}
	// 「キャンセル」時の処理開始
	else{
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false
	}
}

function reactivate(){

	// 「復活ボタン押下」」時の処理開始 ＋ 確認ダイアログの表示
	if(window.confirm('本当にアカウントを復活させますか？')){
		return true;
	}
	// 「キャンセル」時の処理開始
	else{
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false
	}
}
</script>

</head>
<body>
<div class="management-body">

	<div class="main-contents">
		<div class="header">
			<a href="./">ホーム</a>
			<form action="management" method="post">
				<a href="signup">ユーザー新規登録</a>
			</form>
		</div>
	</div>
	<!-- 不正なパラメータが入ってきた場合のエラーメッセージ -->
	<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
    </c:if>
    <!-- end -->
	<div class="account-container">
		<c:if test="${not empty userDatas}">
			<c:forEach items="${userDatas}" var="userData">
					<br>
					アカウント:
					<c:out value="${userData.account}" />
					<input name="account" value="${userData.account}" type="hidden">
					<br>
					名前:
					<c:out value="${userData.name}" />
					<input name="name" value="${userData.name}" type="hidden">
					<br>
					支社:
					<c:out value="${userData.branchName}" />
					<input name="branch" value="${userData.branchName}" type="hidden">
					<br>
					部署:
					<c:out value="${userData.departmentName}" />
					<input name="department" value="${userData.departmentName}" type="hidden">
					<br>

					<div class="userStatus">
						アカウント復活停止状態:
						<c:if test="${userData.isStopped == 0}">稼働中</c:if>
						<c:if test="${userData.isStopped == 1}">停止中</c:if>
					</div>
					<br>
					<form action="setting" method="get">
						<input type="hidden" value="${userData.id}" name="toBeEdUserId"/>
						<input type="submit" value="編集"/>
					</form>

					<c:if test="${loginUser.id != userData.id}">
						<c:choose>
							<c:when test="${userData.isStopped == 1 }">
								<form action="stop" method="post" onsubmit="return reactivate()">
									<input type="hidden" value="${userData.id}" name="userStatusId"/>
									<input type="hidden" value="0" name="userStatus"/>
									<input type="submit" value="復活"/>
								</form>
							</c:when>
							<c:otherwise>
								<form action="stop" method="post" onsubmit="return stop()">
									<input type="hidden" value="${userData.id}" name="userStatusId"/>
									<input type="hidden" value="1" name="userStatus"/>
									<input type="submit" value="停止"/>
								</form>
							</c:otherwise>
						</c:choose>
					</c:if>
					<br>
			</c:forEach>
		</c:if>
	</div>

	<div class="copyright">Copyright(c) Ren Miyamoto</div>
</div>
</body>
</html>