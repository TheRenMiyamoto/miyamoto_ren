<!-- 通常のHTMLファイルではなくJSPであることを以下で示している -->
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<title>ユーザー新規登録</title>
	</head>
	<body>
	<div class="signup-body">
		<div class="main-contents">
			<form action="signup" method="post"><br>
				<div class="signup-header">
					<a href="./management">ユーザー管理画面</a><br>
				</div>
				<div class="signup-main">
					<label for="account">アカウント</label>
                	<input name="account" id="account" value="${account}"/><br>

                	<label for="password">パスワード</label>
               	    <input name="password" type="password" id="password" /><br>

                	<label for="chkPassword">確認用パスワード</label>
                	<input name="chkPassword" type="password" id="chkPassword" /><br>

					<label for="name">名前</label>
					<input name="name" id="name" value="${name}"/><br>

					<c:out value="支社" />
					<select name="branch">
						<c:forEach items="${branches}" var="branch">
							<c:choose>
								<c:when test="${user.branch == branch.id}">
									<option value="${branch.id}" selected><c:out value="${branch.name}"/></option>
								</c:when>
								<c:otherwise>
									<option value="${branch.id}"><c:out value="${branch.name}"/></option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
					<c:out value="部署" />
					<select name="department">
						<c:forEach items="${departments}" var="department">
							<c:choose>
								<c:when test="${user.department == department.id}">
									<option value="${department.id}" selected><c:out value="${department.name}"/></option>
								</c:when>
								<c:otherwise>
									<option value="${department.id}"><c:out value="${department.name}"/></option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
					<input type="submit" value="登録"/><br>
					<a href="./">戻る</a><br>
				</div>
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="errorMessage">
								<li><c:out value="${errorMessage}" />
							</c:forEach>
						</ul>
					</div>
				<c:remove var="errorMessages" scope="session" />
				</c:if>
			</form>
		<div class="copyright">Copyright(c) Ren Miyamoto</div>
		</div>
	</div>
	</body>
</html>