<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./css/style.css" rel="stylesheet" type="text/css">
        <title>ログイン画面</title>
    </head>
    <body>
        <div class="main-contents">

		<!-- LoginFilterのfilterErrorMessagesに値がセットされている場合 -->
        <c:if test="${ not empty filterErrorMessages }">
        		<br>
                <div class="filterErrorMessages">
                	<c:out value="${filterErrorMessages}" />
                </div>
                <br>
        </c:if>

		<!-- LoginServletのerrorMessagesに値がセットされている場合 -->
        <c:if test="${ not empty errorMessages }">
        		<br>
                <div class="errorMessages">
                    <ul>
                    <!-- items = 繰り返し処理の対象となる集合（配列、Mapなど）を指定する。 -->
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
                <br>
            </c:if>

            <form action="login" method="post"><br />
                <label for="account">アカウント</label>
                <input name="account" id="account"/> <br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <input type="submit" value="ログイン" /> <br />
            </form>

            <div class="copyright"> Copyright(c)Ren Miyamoto</div>
        </div>
    </body>
</html>