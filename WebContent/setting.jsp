<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー編集画面</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

			<a href="management">ユーザー管理</a>
			<br>
            <form action="setting" method="post"><br />
                <input name="id" value="${user.id}" id="id" type="hidden"/>

                <label for="account">アカウント名</label>
                <input name="account" value="${user.account}" id="account"/><br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <label for="chkPassword">確認用パスワード</label>
                <input name="chkPassword" type="password" id="chkPassword"/> <br />

                <label for="name">名前</label>
                <input name="name" value="${user.name}" id="name"/><br />

				<c:out value="支社" />
					<select name="branch">
						<c:forEach items="${branches}" var="branch">
							<c:choose>
								<c:when test="user.branch == branch.id">
				 					<option value="${branch.id}"><c:out value="${branch.name}"/></option>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${user.id == loginUser.id}">
											<option value="${branch.id}" disabled><c:out value="${branch.name}"/></option>
										</c:when>
										<c:otherwise>
											<option value="${branch.id}" <c:if test="${user.branch == branch.id}">selected</c:if>><c:out value="${branch.name}"/></option>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				<br>
				<c:out value="部署" />
				<select name="department">
						<c:forEach items="${departments}" var="department">
							<c:choose>
								<c:when test="user.department == department.id">
				 					<option value="${department.id}"><c:out value="${department.name}"/></option>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${user.id == loginUser.id}">
											<option value="${department.id}" disabled><c:out value="${department.name}"/></option>
										</c:when>
										<c:otherwise>
											<option value="${department.id}" <c:if test="${user.department == department.id}">selected</c:if>><c:out value="${department.name}"/></option>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				<br>
                <input type="submit" value="更新" /> <br />
            </form>

            <div class="copyright"> Copyright(c)Ren Miyamoto</div>
        </div>
    </body>
</html>