package beans;

import java.io.Serializable; //Javaのインスタンスをバイト配列に変換できる
import java.util.Date;

public class Department implements Serializable {

	private int id;
    private String name;
    private Date createdDate;
    private Date updatedDate;

    //getterはフィールドの値を取得するメソッド
    //setterはフィールドの値を書き換えるメソッド

  //id
    public int getId() {
 	   return id;
    }

    public void setId(int id) {
 	   this.id = id;
    }

  //name
    public String getName() {
 	   return name;
    }
    public void setName(String name) {
 	   this.name = name;
    }

    //createdDate
    public Date getCreatedDate() {
 	   return createdDate;
    }
    public void setCreatedDate(Date createdDate) {
 	   this.createdDate = createdDate;
    }
    //updatedDate
    public Date getUpdatedDate() {
 	   return updatedDate;
    }
    public void setUpdatedDate(Date updatedDate) {
 	   this.updatedDate = updatedDate;
    }

}