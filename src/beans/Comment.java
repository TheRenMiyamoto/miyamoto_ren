package beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {

    private int id;
    private int userId;
    private int messageId;
    private String text; //MySQLのtext型に対応するのはString型？
    private Date createdDate;
    private Date updatedDate;

    //id
    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }
  //userId
    public int getUserId() {
    	return userId;
    }
    public void setUserId(int userId) {
    	this.userId = userId;
    }
    //messageId
    public int getMessageId() {
    	return messageId;
    }
    public void setMessageId(int messageId) {
    	this.messageId = messageId;
    }
  //text
    public String getText() {
    	return text;
    }
    public void setText(String text) {
    	this.text = text;
    }
    //createdData
    public Date getCreatedDate() {
 	   return createdDate;
    }
    public void setCreatedDate(Date createdDate) {
 	   this.createdDate = createdDate;
    }
    //updatedDate
    public Date getUpdatedDate() {
 	   return updatedDate;
    }
    public void setUpdatedDate(Date updatedDate) {
 	   this.updatedDate = updatedDate;
    }

}

