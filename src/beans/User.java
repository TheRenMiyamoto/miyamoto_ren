package beans;

import java.io.Serializable; //Javaのインスタンスをバイト配列に変換できる
import java.util.Date;

public class User implements Serializable {
	//一旦signup画面の流れに沿っている 4/20
	//9行目から18行目はカプセル化（他クラスからはアクセスできない）
	private int id;
    private String account;
    private String password;
    private String name;
    private int branch;
    private int department;
    private byte isStopped;
    private Date createdDate;
    private Date updatedDate;

    //getterはフィールドの値を取得するメソッド
    //setterはフィールドの値を書き換えるメソッド

  //id
    public int getId() {
 	   return id;
    }

    public void setId(int id) {
 	   this.id = id;
    }

  //account
    public String getAccount() {
 	   return account;
    }
    public void setAccount(String account) {
 	   this.account = account;
    }

  //password
    public String getPassword() {
 	   return password;
    }
    public void setPassword(String password) {
 	   this.password = password;
    }

  //name
    public String getName() {
 	   return name;
    }
    public void setName(String name) {
 	   this.name = name;
    }

  //branch
    public int getBranch() {
 	   return branch;
    }
    public void setBranch(int branch) {
 	   this.branch = branch;
    }

  //department
    public int getDepartment() {
 	   return department;
    }
    public void setDepartment(int department) {
 	   this.department = department;
    }

  //isStopped
    public byte getIsStopped() {
 	   return isStopped;
    }
    public void setIsStopped(byte isStopped) {
 	   this.isStopped = isStopped;
    }

    //createdDate
    public Date getCreatedDate() {
 	   return createdDate;
    }
    public void setCreatedDate(Date createdDate) {
 	   this.createdDate = createdDate;
    }
    //updatedDate
    public Date getUpdatedDate() {
 	   return updatedDate;
    }
    public void setUpdatedDate(Date updatedDate) {
 	   this.updatedDate = updatedDate;
    }

}