package beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {

    private int id;
    private int userId;
    private String title;
    private String category;
    private String text; //MySQLのtext型に対応するのはString型？
    private Date createdDate;
    private Date updatedDate;

    //id
    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }
    //title
    public String getTitle() {
    	return title;
    }
    public void setTitle(String title) {
    	this.title = title;
    }
    //category
    public String getCategory() {
    	return category;
    }
    public void setCategory(String category) {
    	this.category = category;
    }
  //text
    public String getText() {
    	return text;
    }
    public void setText(String text) {
    	this.text = text;
    }
    //userId
    public int getUserId() {
    	return userId;
    }
    public void setUserId(int userId) {
    	this.userId = userId;
    }
    //createdData
    public Date getCreatedDate() {
 	   return createdDate;
    }
    public void setCreatedDate(Date createdDate) {
 	   this.createdDate = createdDate;
    }
    //updatedDate
    public Date getUpdatedDate() {
 	   return updatedDate;
    }
    public void setUpdatedDate(Date updatedDate) {
 	   this.updatedDate = updatedDate;
    }
}

