package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.setCharacterEncoding("UTF-8");

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String start = "2020-01-01 00:00:00";
		String end = sdf.format(new Date());
		String category = null;

		String optedStart = request.getParameter("start");
		String optedEnd = request.getParameter("end");
		String optedCategory = request.getParameter("category");

		if(!StringUtils.isBlank(optedStart)) {
			start = optedStart + " 00:00:00";
		}

		if(!StringUtils.isBlank(optedEnd)) {
			end = optedEnd + " 23:59:59";
		}

		if(!StringUtils.isBlank(optedCategory)) {
			category = "%" + optedCategory + "%";
		}

		List<UserMessage> messages = new MessageService().select(start, end, category);
		List<UserComment> comments = new CommentService().select();

		request.setAttribute("optedStart", optedStart);
		request.setAttribute("optedEnd", optedEnd);
		request.setAttribute("optedCategory", optedCategory);

		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);

		request.getRequestDispatcher("home.jsp").forward(request, response);

	}

}