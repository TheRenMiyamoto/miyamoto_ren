package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		String text = request.getParameter("text");
		int messageId = Integer.parseInt(request.getParameter("id"));

		if (!isValid(text, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		Comment comment = new Comment();
		comment.setText(text);
		comment.setMessageId(messageId);

		User user = (User) session.getAttribute("loginUser");
		int user_id = user.getId();

		comment.setUserId(user_id);
		new CommentService().insert(comment);

		response.sendRedirect("./");
	}

	private boolean isValid(String comment, List<String> errorMessages) {

		if (StringUtils.isBlank(comment)) {
			errorMessages.add("コメントを入力してください");
		} else if (500 < comment.length()) {
			errorMessages.add("コメントを500文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
