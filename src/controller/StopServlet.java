package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//management.jspの復活、停止ボタンが押された時に入ってくる値
		User userStatus = getUserStatus(request);
		new UserService().switchUpdate(userStatus);
		response.sendRedirect("management");
	}

	private User getUserStatus(HttpServletRequest request) throws IOException, ServletException {

		User userStatus = new User();
		userStatus.setId(Integer.parseInt(request.getParameter("userStatusId")));
		userStatus.setIsStopped(Byte.parseByte(request.getParameter("userStatus")));
		return userStatus;
	}
}