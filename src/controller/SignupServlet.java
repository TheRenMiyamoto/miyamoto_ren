package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();
		User user = getUser(request);

		if (!isValid(user, request, errorMessages)) {
			//jspで呼び出すデータの名前を第一引数に指定、第2引数は登録するデータ
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("account", user.getAccount());
			request.setAttribute("name", user.getName());
			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("user", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}
		new UserService().insert(user);
		response.sendRedirect("./management");

	}


	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		//beansクラスのsetterを利用して、ブラウザから入ってきた値をuserにセット
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranch(Integer.parseInt(request.getParameter("branch")));
		user.setDepartment(Integer.parseInt(request.getParameter("department")));

		return user;
	}

	private boolean isValid(User user, HttpServletRequest request, List<String> errorMessages) {

		String account = user.getAccount();
		String password = user.getPassword();
		String chkPassword =  request.getParameter("chkPassword");
		String name = user.getName();
		int branch = user.getBranch();
		int department = user.getDepartment();

		//アカウントのバリデーション

		if (StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");
		} else if(!account.matches("[ -~｡-ﾟ]{6,20}$")){
			errorMessages.add("アカウント名は半角文字で6文字以上20文字以下で入力してください");
		} else if(new UserService().select(account) != null) {
			//上の条件（accountに入ってきたaccount名がバッティングした場合）
			errorMessages.add("アカウント名が重複しています");
		}

		//パスワードのバリデーション
		if (StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードを入力してください");
		} else if(!password.matches("[ -~｡-ﾟ]{6,20}$")) {
			errorMessages.add("パスワードは半角文字で6文字以上20文字以下で入力してください");
		} else if(!password.equals(chkPassword)) {
			errorMessages.add("パスワードと確認パスワードが一致しません");
		}

		//名前のバリデーション
		if(StringUtils.isEmpty(name)) {
			errorMessages.add("名前を入力してください");
		} else if(10 < name.length()) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		//支社&部署のバリデーション
		if(branch == 1 && (department >= 3)) {
			errorMessages.add("本社に存在しない部署が選択されています");
		} else if((branch >= 2 && branch <= 4) && (department >= 1 && department <= 2)) {
			errorMessages.add("支社に存在しない部署が選択されています");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}