package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;


@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String text = request.getParameter("text");
		Message message = toMessage(title, category, text);

		if (!isValid(title, category, text, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("message.jsp").forward(request, response);
			return;

		} else {
			User user = (User) session.getAttribute("loginUser");
			int userId = user.getId();
			message.setUserId(userId);
			new MessageService().insert(message);
			response.sendRedirect("./");
		}
	}

	public Message toMessage(String title, String category, String text) {
		Message message = new Message();
		message.setTitle(title);
		message.setCategory(category);
		message.setText(text);
		return message;

	}

	private boolean isValid(String title, String category,String text, List<String> errorMessages) {


		//タイトルのバリデーション
		if (StringUtils.isBlank(title)) {
			errorMessages.add("件名を入力してください");
		} else if (30 < title.length()) {
			errorMessages.add("件名を30文字以下で入力してください");
		}

		//カテゴリーのバリデーション
		if (StringUtils.isBlank(category)) {
			errorMessages.add("カテゴリーを入力してください");
		} else if (10 < category.length()) {
			errorMessages.add("カテゴリーを10文字以下で入力してください");
		}

		//テキストのバリデーション
		if (StringUtils.isBlank(text)) {
			errorMessages.add("本文を入力してください");
		} else if (1000 < text.length()) {
			errorMessages.add("本文を1000文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}

