package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String toBeEdUserId = request.getParameter("toBeEdUserId");
		List<String> errorMessages = new ArrayList<String>();

		//不正なパラメータ(数字以外)のバリデーション
		if(StringUtils.isEmpty(toBeEdUserId) || !toBeEdUserId.matches("^[0-9]*$")) {
			errorMessages.add("不正なパラメータが入力されました");
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("management").forward(request, response);
		}

		User user = new UserService().select(Integer.parseInt(toBeEdUserId));

		//不正なパラメータ(存在しないユーザー)のバリデーション
		if(user == null) {
			errorMessages.add("不正なパラメータが入力されました");
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("management").forward(request, response);
		}


		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();
		request.setAttribute("user", user);
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("setting.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		User user = getUser(request);
		List<String> errorMessages = new ArrayList<String>();
		if (!isValid(user, request, errorMessages)) {

			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();

			request.setAttribute("user", user);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("setting.jsp").forward(request, response);

		} else {
			new UserService().update(user);
			response.sendRedirect("management");
		}
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();

		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranch(Integer.parseInt(request.getParameter("branch")));
		user.setDepartment(Integer.parseInt(request.getParameter("department")));

		return user;
	}

	private boolean isValid(User user,HttpServletRequest request, List<String> errorMessages) {

		String account = user.getAccount();
		String password = user.getPassword();
		String chkPassword =  request.getParameter("chkPassword");
		String name = user.getName();
		int branch = user.getBranch();
		int department = user.getDepartment();

		//アカウントのバリデーション
		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");
		} else if(!account.matches("[ -~｡-ﾟ]{6,20}$")){
			errorMessages.add("アカウント名は半角文字で6文字以上20文字以下で入力してください");
		} else  {
			User dbUsers = new UserService().select(account);
			if(dbUsers != null && dbUsers.getId() != user.getId()) {
				errorMessages.add("アカウント名が重複しています");
			}
		}

		//パスワードのバリデーション
		if (StringUtils.isBlank(password) && StringUtils.isBlank(chkPassword)) {
		} else {

			if(!password.equals(chkPassword)) {
				errorMessages.add("パスワードと確認パスワードが一致しません");
			} else if(!password.matches("[ -~｡-ﾟ]{6,20}$")) {
				errorMessages.add("パスワードは半角文字で6文字以上20文字以下で入力してください");
			}
		}
		//名前のバリデーション
		if(StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください");
		} else if(10 < name.length()) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		//支社&部署のバリデーション
		if(branch == 1 && (department == 3 || department == 4)) {
			//branchの値が1の時にdepartmentの値が3~4だった場合
			errorMessages.add("本社に存在しない部署が選択されています");
		} else if((branch == 2 || branch == 3 || branch == 4) && (department == 1 || department == 2)) {
			//branchの値が2~4の時にdepartmentの値が1~2だった場合
			errorMessages.add("支社に存在しない部署が選択されています");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}