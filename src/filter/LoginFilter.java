package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		User loginUser = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");
		List<String> filterErrorMessages = new ArrayList<>();

		if (loginUser != null || ((HttpServletRequest) request).getServletPath().equals("/login")) {
			//セッションがNullではない場合（ログイン済の場合）は通常通り画面を遷移させる
			chain.doFilter(request, response);// サーブレットを実行

		} else {
			//セッションがNullの場合（ログイン未の場合）は、ログイン画面をリダイレクト
			filterErrorMessages.add("ログインしてください");
			((HttpServletRequest) request).getSession().setAttribute("filterErrorMessages", filterErrorMessages);
			((HttpServletResponse) response).sendRedirect("login");
		}

	}

	@Override
	public void init(FilterConfig config) throws ServletException{
	}

	@Override
	public void destroy() {
	}

}
