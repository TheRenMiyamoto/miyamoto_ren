package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import beans.User;

@WebFilter(urlPatterns = {"/management", "/signup", "/setting"})
public class ManagementFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		User loginUser = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");
		//loginUserの支社IDを取得
		int userBranchId = loginUser.getBranch();
		//loginUserの部署IDを取得
		int userDepartmetnId = loginUser.getDepartment();

		List<String> managementErrorMessages = new ArrayList<>();

		if (userBranchId == 1 && userDepartmetnId == 1) {
			chain.doFilter(request, response);// サーブレットを実行

		} else {
			managementErrorMessages.add("ユーザー管理画面にアクセスする権限がありません");
			request.setAttribute("managementErrorMessages", managementErrorMessages);
			request.getRequestDispatcher("./").forward(request, response);
		}

	}

	@Override
	public void init(FilterConfig config) throws ServletException{
		//とりあえず空でよい
	}

	@Override
	public void destroy() {
		//とりあえず空でよい
	}

}
