package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;

public class BranchDao {

	public List<Branch> select(Connection connection) {

		//statementはSQL文（以下プログラム）をデータベース（MySQL）を送る箱の役割
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("    branches.id as id, ");
			sql.append("    branches.name as name ");
			sql.append("FROM branches ");
			sql.append("ORDER BY created_date DESC");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();

			//実行結果をtop.jspを経由してブラウザで表示
			List<Branch> branches = toBranches(rs);
			return branches;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<Branch> toBranches(ResultSet rs) throws SQLException {

		List<Branch> branches = new ArrayList<Branch>();
		try {
			//ResultSetインターフェースのnextメソッドを使用し、カーソルを現在の位置から1行下に移動します
			while (rs.next()) {
				Branch branch = new Branch();
				branch.setId(rs.getInt("id"));
				branch.setName(rs.getString("name"));

				branches.add(branch);
			}
			return branches;
		} finally {
			close(rs);
		}
	}
}