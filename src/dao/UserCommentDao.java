package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> select(Connection connection) {

		//statementはSQL文（以下プログラム）をデータベース（MySQL）を送る箱の役割
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("    comments.id as id, ");
			sql.append("    comments.text as text, ");
			sql.append("    comments.user_id as user_id, ");
			sql.append("    comments.message_id as message_id, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name, ");
			sql.append("    comments.created_date as created_date ");
			sql.append("FROM comments ");
			//commentsテーブルとusersテーブルを内部結合するSQL文
			sql.append("INNER JOIN users ");
			//comments.user_idとusers.user.idの紐づけ
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date ASC");

			//初期値（null)に前述プログラム（SELECT文をsqlへappend）を文字列にしてpsに格納
			ps = connection.prepareStatement(sql.toString());

			//psをMySQLで実行し、rsへ格納
			ResultSet rs = ps.executeQuery();

			//実行結果をhome.jspを経由してブラウザで表示
			List<UserComment> comments = toUserComments(rs);
			return comments;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserComments(ResultSet rs) throws SQLException {

		List<UserComment> comments = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				UserComment comment = new UserComment();
				comment.setId(rs.getInt("id"));
				comment.setText(rs.getString("text"));
				comment.setUserId(rs.getInt("user_id"));
				comment.setMessageId(rs.getInt("message_id"));
				comment.setAccount(rs.getString("account"));
				comment.setName(rs.getString("name"));
				comment.setCreatedDate(rs.getTimestamp("created_date"));

				comments.add(comment);
			}
			return comments;

		} finally {
			close(rs);
		}
	}
}