package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Management;
import exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	public List<Management> select(Connection connection) {

		//statementはSQL文（以下プログラム）をデータベース（MySQL）を送る箱の役割
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("    users.id as id, ");
			sql.append("    users.account as account,");
			sql.append("    users.name as name,");
			sql.append("    users.branch_id as branch_id, ");
			sql.append("    users.department_id as department_id, ");
			sql.append("    users.created_date as created_date, ");
			sql.append("    users.updated_date as updated_date, ");
			sql.append("    users.is_stopped as is_stopped, ");
			sql.append("    branches.name as branch_name, ");
			sql.append("    departments.name as department_name ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("ORDER BY created_date DESC");

			//初期値（null)に前述プログラム（SELECT文をsqlへappend）を文字列にしてpsに格納
			ps = connection.prepareStatement(sql.toString());

			//psをMySQLで実行し、rsへ格納
			ResultSet rs = ps.executeQuery();
			System.out.println(rs);
			//実行結果をmanagement.jspを経由してブラウザで表示A
			List<Management> userDatas = toUserData(rs);
			return userDatas;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Management> toUserData(ResultSet rs) throws SQLException {

		List<Management> userDatas = new ArrayList<Management>();
		try {
			while (rs.next()) {
				Management userData = new Management();
				userData.setId(rs.getInt("id"));
				userData.setName(rs.getString("name"));
				userData.setAccount(rs.getString("account"));
				userData.setBranchId(rs.getInt("branch_id"));
				userData.setDepartmentId(rs.getInt("department_id"));
				userData.setCreatedDate(rs.getTimestamp("created_date"));
				userData.setUpdatedDate(rs.getTimestamp("updated_date"));
				userData.setBranchName(rs.getString("branch_name"));
				userData.setDepartmentName(rs.getString("department_name"));
				userData.setIsStopped(rs.getByte("is_stopped"));

				userDatas.add(userData);
			}
			return userDatas;

		} finally {
			close(rs);
		}
	}


}