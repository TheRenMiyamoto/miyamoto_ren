package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Department;
import exception.SQLRuntimeException;

public class DepartmentDao {

	public List<Department> select(Connection connection) {

		//statementはSQL文（以下プログラム）をデータベース（MySQL）を送る箱の役割
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("    departments.id as id, ");
			sql.append("    departments.name as name ");
			sql.append("FROM departments ");
			sql.append("ORDER BY created_date DESC");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();

			//実行結果をtop.jspを経由してブラウザで表示
			List<Department> departments = toDepartments(rs);
			System.out.println(rs);
			return departments;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<Department> toDepartments(ResultSet rs) throws SQLException {

		List<Department> departments = new ArrayList<Department>();
		try {
			//ResultSetインターフェースのnextメソッドを使用し、カーソルを現在の位置から1行下に移動します
			while (rs.next()) {
				Department department = new Department();
				department.setId(rs.getInt("id"));
				department.setName(rs.getString("name"));

				departments.add(department);
			}
			return departments;
		} finally {
			close(rs);
		}
	}
}