package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		//statementはSQL文（以下プログラム）をデータベース（MySQL）を送る箱の役割
		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("INSERT INTO users ( ");
			sql.append("    account, ");
			sql.append("    password, ");
			sql.append("    name, ");
			sql.append("    branch_id, ");
			sql.append("    department_id, ");
			sql.append("    created_date, ");
			sql.append("    updated_date ");
			sql.append(") VALUES ( ");
			sql.append("    ?, ");                                  // account
			sql.append("    ?, ");                                  // password
			sql.append("    ?, ");                                  // name
			sql.append("    ?, ");                                  // branch_id
			sql.append("    ?, ");                                  // department_id
			sql.append("    CURRENT_TIMESTAMP, ");      // created_date
			sql.append("    CURRENT_TIMESTAMP ");       // updated_date
			sql.append(")");

			//前述プログラム（SELECT文をsqlへappend）を文字列にしてpsに格納
			ps = connection.prepareStatement(sql.toString());

			//ブラウザ側から登録してきた値が入ってきた値をバインド変数にセット
			ps.setString(1, user.getAccount());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getDepartment());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User select(Connection connection, String account, String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, account);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);
			if (users.isEmpty()) {
				return null;
			} else if (2 <= users.size()) {
				throw new IllegalStateException("ユーザーが重複しています");
			} else {
				return users.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUsers(ResultSet rs) throws SQLException {

		List<User> users = new ArrayList<User>();
		try {
			//ResultSetインターフェースのnextメソッドを使用し、カーソルを現在の位置から1行下に移動します
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setAccount(rs.getString("account"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranch(Integer.parseInt(rs.getString("branch_id")));
				user.setDepartment(Integer.parseInt(rs.getString("department_id")));
				user.setIsStopped(Byte.parseByte(rs.getString("is_stopped")));
				user.setCreatedDate(rs.getTimestamp("created_date"));
				user.setUpdatedDate(rs.getTimestamp("updated_date"));

				users.add(user);
			}
			return users;
		} finally {
			close(rs);
		}
	}

	public User select(Connection connection, String account) {

		//statementはSQL文（以下プログラム）をデータベース（MySQL）を送る箱の役割
		PreparedStatement ps = null;
		try {
			//MySQLで実行するSQL文をString型の変数(sql)へ格納
			String sql = "SELECT * FROM users WHERE account = ?";

			//sql文をpsへ格納
			ps = connection.prepareStatement(sql);
			//sql文のバインド変数に引数（account）に入ってきた値をセット
			ps.setString(1, account);

			//ここでsql文を実行
			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);
			if(users.isEmpty()) {
				return null;
			} else if(2 <= users.size()) {
				throw new IllegalStateException("ユーザーが重複しています");
			} else {
				return users.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//ユーザー編集用(ユーザーID参照用)
	public User select(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);

			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);
			//ユーザー重複の条件分岐
			if (users.isEmpty()) {
				return null;
			} else if (2 <= users.size()) {
				throw new IllegalStateException("ユーザーが重複しています");
			} else {
				return users.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//ユーザー編集用(更新用)
	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("    account = ?, ");
			sql.append("    name = ?, ");
			sql.append("    branch_id = ?, ");
			sql.append("    department_id = ?, ");
			if(!StringUtils.isEmpty(user.getPassword())) {
				sql.append("    password = ?, ");
			}
			sql.append("    updated_date = CURRENT_TIMESTAMP ");
			sql.append("WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch());
			ps.setInt(4, user.getDepartment());
			if(!StringUtils.isEmpty(user.getPassword())) {
				ps.setString(5, user.getPassword());
				ps.setInt(6, user.getId());
			} else {
				ps.setInt(5, user.getId());
			}
			//excuteUpdate();でUPDATE文を実行する
			int count = ps.executeUpdate();

			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//停止ボタン用(更新用)
	public void switchUpdate(Connection connection, User userStatus) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("    users.is_stopped = ?, ");
			sql.append("    updated_date = CURRENT_TIMESTAMP ");
			sql.append("WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setByte(1, userStatus.getIsStopped());
			ps.setInt(2, userStatus.getId());

			//excuteUpdate();でUPDATE文を実行する
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}