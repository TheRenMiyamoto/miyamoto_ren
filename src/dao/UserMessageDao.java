package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> select(Connection connection, String start, String end, String category) {

		//statementはSQL文（以下プログラム）をデータベース（MySQL）を送る箱の役割
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("    messages.id as id, ");
			sql.append("    messages.title as title, ");
			sql.append("    messages.text as text, ");
			sql.append("    messages.category as category, ");
			sql.append("    users.id as user_id, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name, ");
			sql.append("    messages.created_date as created_date ");
			sql.append("FROM messages ");
			//messagesテーブルとusersテーブルを内部結合するSQL文
			sql.append("INNER JOIN users ");
			//messages.user_idとusers.user.idの紐づけ
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE messages.created_date BETWEEN ? AND ? ");
			if(category != null) {
				sql.append("AND messages.category LIKE ? ");
			}
			sql.append("ORDER BY created_date DESC");

			//初期値（null)に前述プログラム（SELECT文をsqlへappend）を文字列にしてpsに格納
			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, start);
			ps.setString(2, end);
			if(category != null) {
				ps.setString(3, category);
			}


			ResultSet rs = ps.executeQuery();

			//実行結果をhome.jspを経由してブラウザで表示
			List<UserMessage> messages = toUserMessages(rs);
			return messages;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

		List<UserMessage> messages = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				UserMessage message = new UserMessage();
				message.setId(rs.getInt("id"));
				message.setTitle(rs.getString("title"));
				message.setText(rs.getString("text"));
				message.setCategory(rs.getString("category"));
				message.setUserId(rs.getInt("user_id"));
				message.setAccount(rs.getString("account"));
				message.setName(rs.getString("name"));
				message.setCreatedDate(rs.getTimestamp("created_date"));

				messages.add(message);
			}
			return messages;

		} finally {
			close(rs);
		}
	}
}